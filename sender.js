/* for testing you can change path of receiver and file name before running */

var io = require('socket.io-client');
var ss = require('socket.io-stream');
var socket = io.connect('http://127.0.0.1:3000');
var fs = require('fs');
var stream = ss.createStream();
var path = require('path');
socket.on('connect', function(){
  socket.emit('sendpath',{path:path.join(__dirname,'save')});
  socket.on('ready',function(data,error){
  	var filename = 'SQLManagementStudio_x64_ENU.exe';
  	console.info('start sending file');
  	ss(socket).emit('sendfile', stream, {name: filename, size : fs.statSync(filename).size});
  	fs.createReadStream(filename).pipe(stream);
  });
  socket.on('finish',function(data,error){
  	if(data.status)
  		console.log('done');
  	else
  		console.log('error');
  	socket.disconnect();
  })
});


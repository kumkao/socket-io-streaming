'use strict';
var io = require('socket.io').listen(3000);
var ss = require('socket.io-stream');
var path = require('path');
var fs = require('fs');

io.sockets.on('connection', function(socket){
  var savePath;
  var fstream;
  console.info('connecting from',socket.conn.remoteAddress,'.....');
  socket.on('sendpath',function(data,error)
  {
    console.info('path',data.path,'was saved');
    savePath = data.path;
    socket.emit('ready',{});
  });
  ss(socket).on('sendfile', function(stream, data) {
    console.info('start receiving data');
    var fullPath = path.join(savePath,data.name);
    var srcSize = data.size;
    fstream = stream.pipe(fs.createWriteStream(fullPath));
    fstream.on('close', function() {
        console.log("CLOSE");
    });
    fstream.on('error', function(err) {
        console.log("ERROR:" + err);
        fstream.end();
        fs.unlink(fullPath,function(error){
          console.log(error);
        });
        socket.emit('finish',{status:false});
    });
    fstream.on('finish', function() {
      var dstSize = fs.statSync(fullPath).size;
      console.log('source file size',srcSize,'destination file size',dstSize);
      if(dstSize != srcSize)
      {
        console.log('receive file failed, ack for disconnecting');
       
        fs.unlink(fullPath,function(error){
          console.log(error);
        });
        socket.emit('finish',{status:false});
      }
      else
      {
        console.log('receive file succussfully, ack for disconnecting');
        socket.emit('finish',{status:true});
      }
      
    });
  });
  socket.on('disconnect', function() {
      console.log('Got disconnect!');
      fstream.end();
   });
});